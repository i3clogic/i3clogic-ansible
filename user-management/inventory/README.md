Inventory
=========

Place to store your inventory files for different clusters. By default, there are two sample inventory files:

* **production.ini** - It contains sample hosts for production servers

Refer the sample inventory files for more details.
