#! /bin/bash
#PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

#a=`uptime | awk -F ":" '{print $5}' | awk -F "," '{print $1}' | awk -F "." '{print $1}'`
currentload=`uptime | awk -F ":" '{print $5}' | awk -F "," '{print $1}'`
core=`nproc`
percantage=`expr "$currentload * 100 / $core" |bc -l | awk -F "." '{print$1}'`
load=`/usr/lib64/nagios/plugins/check_load -w 5,3,1 -c 7,6,5 |sed 's/.*- //g'`
if [[ "$percantage" -lt "75" ]]
then
echo "OK : $load"
exit 0
elif [[ "$percantage" -gt 75  && "$percantage" -lt 85 ]]
then
echo "Warning : $load"
exit 1
else
echo "Critical: $load"
exit 2
fi


