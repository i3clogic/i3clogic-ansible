echo "This script requires superuser access to install td-agent packages."
echo "You will be prompted for your password by sudo."


# run inside sudo
sudo sh <<SCRIPT
  curl -L http://toolbelt.treasuredata.com/sh/install-redhat-td-agent2.sh | sh
SCRIPT


